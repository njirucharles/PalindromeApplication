package com.java.palindrome.controllers;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/palindrome")
public class PalindromeController {

    @GetMapping("{word}")
    @JsonProperty("jsonData")
    public String get(@PathVariable String word) throws JSONException {
        String rev = "";
        String status;
        JSONObject results = new JSONObject();
        int length = word.length();

        for (int i = length - 1; i >= 0; i--)
            rev = rev + word.charAt(i);

        if (word.equals(rev))
            status = "Word is a palindrome";
        else
            status = "not a palindrome";

        results.put("Word", word);
        results.put("NewWord", rev);
        results.put("Status", status);
        return " result " + results.toString();
    }
}